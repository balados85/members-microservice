<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use APP\Member;

class Member extends Model
{
    protected $fillable = [
    	'first_name',
    	'middle_name',
    	'last_name',
    	'date_of_birth',
    	'gender',
    	'postal_address',
    	'residential_address',
    	'city',
    	'country',
    	'has_dependents',
    	'company',
    	'secondary_number',
    	'primary_number',
    	'employer',
    	'email',
        'benefit_id',
        'image_dir',
        'status'
	];

	public function dependents(){

		 return $this->hasMany(Dependent::class);
	}
}
