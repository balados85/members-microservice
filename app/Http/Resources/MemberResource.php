<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MemberResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
            'id' => $this->id,
            'firstName' => $this->first_name,
            'middle_name' => $this->middle_name,
            'lastName' => $this->last_name,
            'dateOfBirth' => (string)$this->date_of_birth,
            'gender' => $this->gender,
            'email' => $this->email,
            'addresses' => [
                'postalAddress' => $this->postal_address,
                'residentialAddress' => $this->residential_address,
                'city' => $this->city,
                'coutry' => $this->country,
            ],
            'hasDependent' => $this->has_dependents,
            'dependents' => DependentResource::collection($this->dependents),
            'contact' => [
                'primaryNumber' => $this->primary_number,
                'secondaryNumber' => $this->secondary_number,
            ],
            'status' => $this->status


        ];
       
    }


}
