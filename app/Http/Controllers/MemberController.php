<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Member;

use App\Http\Resources\MemberResource;

use App\Http\Resources\MemberResourceCollection;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         //$members = Member::all();

         //return response()->json($members);
        return new MemberResourceCollection(Member::all());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $member = new Member();
        $member->first_name = $request->input('first_name');
        $member->middle_name = $request->input('middle_name');
        $member->last_name = $request->input('last_name');
        $member->date_of_birth = $request->input('date_of_birth');
        $member->gender = $request->input('gender');
        $member->postal_address = $request->input('postal_address');
        $member->residential_address = $request->input('residential_address');
        $member->city = $request->input('city');
        $member->country = $request->input('country');
        $member->has_dependents = $request->input('has_dependents');
        $member->company = $request->input('company');
        $member->secondary_number = $request->input('secondary_number');
        $member->primary_number = $request->input('primary_number');
        $member->employer = $request->input('employer');
        $member->email = $request->input('email');
        $member->benefit_id = $request->input('benefit_id');
        $member->image_dir = $request->input('image_dir');

        $member->save();

        return response()->json($member);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$member = Member::find($id);
        //eturn response()->json($member);
        return new MemberResource(Member::FindOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $member = Member::find($id);
        $member->first_name = $request->input('first_name');
        $member->middle_name = $request->input('middle_name');
        $member->last_name = $request->input('last_name');
        $member->date_of_birth = $request->input('date_of_birth');
        $member->gender = $request->input('gender');
        $member->postal_address = $request->input('postal_address');
        $member->residential_address = $request->input('residential_address');
        $member->city = $request->input('city');
        $member->country = $request->input('country');
        $member->has_dependents = $request->input('has_dependents');
        $member->company = $request->input('company');
        $member->secondary_number = $request->input('secondary_number');
        $member->primary_number = $request->input('primary_number');
        $member->employer = $request->input('employer');
        $member->email = $request->input('email');
        $member->benefit_id = $request->input('benefit_id');
        $member->image_dir = $request->input('image_dir');


        $member->save();

        return response()->json($member);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::find($id);
        $member->delete();
        return response()->json("Success");
    }
}
