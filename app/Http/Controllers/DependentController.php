<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Dependent;

class DependentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $dependents = Dependent::all();

         return response()->json($dependents);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dependent = new Dependent();
        $dependent->first_name = $request->input('first_name');
        $dependent->middle_name = $request->input('middle_name');
        $dependent->last_name = $request->input('last_name');
        $dependent->date_of_birth = $request->input('date_of_birth');
        $dependent->gender = $request->input('gender');
        $dependent->residential_address = $request->input('residential_address');
        $dependent->company = $request->input('company');
        $dependent->contact = $request->input('contact');
        $dependent->member_id = $request->input('member_id');
        $dependent->email = $request->input('email');
        $dependent->benefit_id = $request->input('benefit_id');
        $dependent->image_dir = $request->input('image_dir');


        $dependent->save();

        return response()->json($dependent);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dependents = Dependent::find($id);
        return response()->json($dependents);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dependent = Dependent::find($id);
        $dependent->first_name = $request->input('first_name');
        $dependent->middle_name = $request->input('middle_name');
        $dependent->last_name = $request->input('last_name');
        $dependent->date_of_birth = $request->input('date_of_birth');
        $dependent->gender = $request->input('gender');
        $dependent->residential_address = $request->input('residential_address');
        $dependent->company = $request->input('company');
        $dependent->contact = $request->input('contact');
        $dependent->member_id = $request->input('member_id');
        $dependent->email = $request->input('email');
        $dependent->benefit_id = $request->input('benefit_id');
        $dependent->image_dir = $request->input('image_dir');

        $dependent->save();

        return response()->json($dependent);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $dependent = Dependent::find($id);
        $dependent->delete();
        return response()->json("Success");
    }
}
