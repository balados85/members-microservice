<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dependent extends Model
{
   protected $fillable = [
    	'first_name',
    	'middle_name',
    	'last_name',
    	'date_of_birth',
    	'gender',
    	'residential_address',
    	'company',
    	'contact',
    	'email',
        'member_id',
        'benefit_id',
        'image_dir',
        'status'
	];

	public function member(){

		 return $this->belongsTo(Member::class);
	}
}
