<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/v1/member/', 'MemberController@index');

Route::get('/v1/member/get/{id}', 'MemberController@show');

Route::post('/v1/member/create','MemberController@store');

Route::put('/v1/member/update/{id}','MemberController@update');

Route::delete('/v1/member/delete/{id}','MemberController@destroy');


Route::get('/v1/dependent/', 'DependentController@index');

Route::get('/v1/dependent/get/{id}', 'DependentController@show');

Route::post('/v1/dependent/create','DependentController@store');

Route::put('/v1/dependent/update/{id}','DependentController@update');

Route::delete('/v1/dependent/delete/{id}','DependentController@destroy');