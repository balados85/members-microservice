<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->date('date_of_birth');
            $table->enum('gender', array('MALE', 'FEMALE', 'UNKNOWN'));
            $table->string('postal_address')->nullable();
            $table->string('residential_address')->nullable();
            $table->string('city');
            $table->string('country');
            $table->boolean('has_dependents');
            $table->bigInteger('company');
            $table->string('secondary_number')->nullable();
            $table->string('primary_number')->unique();
            $table->bigInteger('employer')->nullable();
            $table->string('email')->unique();
            $table->bigInteger('benefit_id');
            $table->string("image_dir");
            $table->enum("status", array('ACTIVE', 'INACTIVE','NOT ACTIVATED'));


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
