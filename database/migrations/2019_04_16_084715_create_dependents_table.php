<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDependentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dependents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->date('date_of_birth');
            $table->enum('gender', array('MALE', 'FEMALE', 'UNKNOWN'));
            $table->string('residential_address')->nullable();
            $table->bigInteger('company');
            $table->string('contact')->nullable()->unique();
            $table->string('email')->unique()->nullable();
            $table->bigInteger('member_id');
            $table->bigInteger('benefit_id');
            $table->string("image_dir");          
            $table->enum("status", array('ACTIVE', 'INACTIVE','NOT ACTIVATED'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dependents');
    }
}
